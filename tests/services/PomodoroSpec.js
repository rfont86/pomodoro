import Pomodoro from '../../js/services/Pomodoro.js'
import Bus from '../../js/libraries/Bus.js'
import Callback from '../Callback.js'

describe('Pomodoro service', () => {
  let bus = null

  beforeEach(() => {
    bus = new Bus()
  })

  it('subscribes to start event', () => {
    spyOn(bus, 'subscribe')

    const service = new Pomodoro(bus)

    expect(bus.subscribe).toHaveBeenCalledWith('timer.start_requested', aCallback())
  })

  it('on start, publishes start event with initial time left', () => {
    const callback = new Callback(bus, 'timer.start')
    const initialTimeLeft = { isPaused: false, minutes: 25 }
    const service = new Pomodoro(bus)

    service.start()

    expect(callback.hasBeenCalledWith()).toEqual(initialTimeLeft)
  })

  it('does not send a negative time left', () => {
    const callback = new Callback(bus, 'timer.timeLeft')
    const zeroTimeLeft = { isPaused: false, minutes: 0 }
    const service = new Pomodoro(bus)
    service.start()

    for (let times = 0; times < 100; times++) {
       service.calculateTimeLeft()
     }

    expect(callback.hasBeenCalledWith()).toEqual(zeroTimeLeft)
  })

  it('after pause the count down the time left does not changes', () => {
    const pausedTimeLeft = { isPaused: true, minutes: 24 }
    const callback = new Callback(bus, 'timer.timeLeft')
    const service = new Pomodoro(bus)
    service.start()
    service.calculateTimeLeft()

    service.pause()

    service.calculateTimeLeft()
    expect(callback.hasBeenCalledWith()).toEqual(pausedTimeLeft)
  })

  it('announces when is paused', () => {
    const pausedTimeLeft = { isPaused: true, minutes: 0 }
    const callback = new Callback(bus, 'timer.paused')
    const service = new Pomodoro(bus)

    service.pause()

    expect(callback.hasBeenCalledWith()).toEqual(pausedTimeLeft)
  })

  it('announces when is reseted', () => {
    const pausedTimeLeft = { isPaused: true, minutes: 25 }
    const callback = new Callback(bus, 'timer.reseted')
    const service = new Pomodoro(bus)

    service.reset()

    expect(callback.hasBeenCalledWith()).toEqual(pausedTimeLeft)
  })

  it('can resume the countdown', () => {
    const resumedTimeLeft = { isPaused: false, minutes: 23 }
    const callback = new Callback(bus, 'timer.timeLeft')
    const service = new Pomodoro(bus)
    service.start()
    service.calculateTimeLeft()
    service.pause()

    service.start()

    service.calculateTimeLeft()
    expect(callback.hasBeenCalledWith()).toEqual(resumedTimeLeft)
  })

  function aCallback() {
    return jasmine.any(Function)
  }
})
