import Pomodoro from '../../js/components/Pomodoro.js'
import Bus from '../../js/libraries/Bus.js'
import Callback from '../Callback.js';

describe("Pomodoro component", () => {
  let document, bus, container = null

  beforeEach(() => {
    document = new FakeDocument()
    bus = new Bus()
    container = { innerHTML: {} }
  })

  it("renders a timer in the given container", () => {
    const pomodoro = new Pomodoro(container, bus, document)

    pomodoro.draw()

    expect(container.innerHTML).toContain("time")
  })

  it("starts at 00", () => {
    const pomodoro = new Pomodoro(container, bus, document)

    pomodoro.draw()

    expect(container.innerHTML).toContain("00")
  })

  it("renders a start button", () => {
    const pomodoro = new Pomodoro(container, bus, document)

    pomodoro.draw()

    expect(container.innerHTML).toContain("Start")
  })

  it('renders time left after pass time', () => {
    new Pomodoro(container, bus, document)

    bus.publish('timer.timeLeft', { minutes: 24 })

    expect(container.innerHTML).toContain("24")
  })

  it('asks for pause the countdown', () => {
    const callback = new Callback(bus, 'timer.pause')
    const pomodoro = new Pomodoro(container, bus, document)

    pomodoro.pause()

    expect(callback.hasBeenCalled()).toEqual(true)
  })

  it('asks for reset the countdown', () => {
    const callback = new Callback(bus, 'timer.reset')
    const pomodoro = new Pomodoro(container, bus, document)

    pomodoro.reset()

    expect(callback.hasBeenCalled()).toEqual(true)
  })

  class FakeDocument {
    querySelector(_) {
      return {}
    }
  }
})
