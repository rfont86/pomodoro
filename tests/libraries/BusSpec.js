import Bus from '../../js/libraries/Bus.js'
import Callback from '../Callback.js'

describe('Bus service', () => {
  it('can publish a topic without subscriptions', () => {
    const bus = new Bus()
    const aTopic = 'this.is.a.topic'
    const aMessage = 'this is a message'

    expect(() => {
      bus.publish(aTopic, aMessage)
    }).not.toThrow(TypeError)
  })

  it('executes all the actions subscribed to a topic when it is published with a message', () => {
    const message = 'Any message'
    const sameTopic = 'a.topic'
    const bus = new Bus()
    const callback = new Callback(bus, sameTopic)

    bus.publish(sameTopic, message)

    expect(callback.hasBeenCalledWith()).toEqual(message)
  })
})
