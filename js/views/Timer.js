const DISABLED_START_BUTTON = '<button id="start" disabled>Start</button>'
const START_BUTTON = '<button id="start">Start</button>'
const NO_TIME_LEFT = 0

const ENABLED_PAUSE_BUTTON = '<button id="pause">Pause</button>'
const DISABLED_PAUSE_BUTTON = '<button id="pause" disabled>Pause</button>'
const ENABLED_RESET_BUTTON = '<button id="reset">Reset</button>'
const DISABLED_RESET_BUTTON = '<button id="reset" disabled>Reset</button>'

class Timer {
  constructor(document) {
    this.document = document
    

  }

  render(properties) {
    let minutes = properties.minutes
    let isPaused = properties.isPaused
    console.log(minutes, isPaused)

    return `
      <div id="container">
        <div id="buttons">
          ${this._startButton(minutes, isPaused)}
          ${this._pauseButton(minutes, isPaused)}
          ${this._resetButton(minutes)}
        </div>
        <div id="timer">
          <div id="time">
            <span id="minutes">${this._format(minutes)}</span>
          </div>
          <div id="filler"></div>
        </div>
      </div>
    `
  }

  addCallbacks(callbacks) {
    this._addOnClickToStart(callbacks.startCountDown)
    this._addOnClickToPause(callbacks.pause)
    this._addOnClickToReset(callbacks.reset)
  }

  _format(number) {
    let result = number.toString()

    if (this._thereIsADigit(number)) { result = `0${result}` }

    return result
  }

  _startButton(countdown, isPaused) {
    
    if(this._isRunning(countdown, isPaused)) { return DISABLED_START_BUTTON }

    return START_BUTTON
  }

  _pauseButton(minutes, isPaused)   {
    
    if(!this._isRunning(minutes, isPaused)){return DISABLED_PAUSE_BUTTON}
    
    return ENABLED_PAUSE_BUTTON
  }

  _resetButton(minutes) {
    
    if(!this._isStarted(minutes)){return DISABLED_RESET_BUTTON}

    return ENABLED_RESET_BUTTON
  }

  _addOnClickToStart(callback) {
    this._addOnClickTo('#start', callback)
  }

  _addOnClickToPause(callback) {
    this._addOnClickTo('#pause', callback)
  }

  _addOnClickToReset(callback) {
    this._addOnClickTo('#reset', callback)
  }

  _addOnClickTo(id, callback) {
    const element = this.document.querySelector(id)
    element.onclick = callback
  }

  _isRunning(minutes, isPaused) {
    return ((minutes > NO_TIME_LEFT) && !isPaused)
  }

  _isStarted(minutes) {
    return (minutes > NO_TIME_LEFT)
  }


  _thereIsADigit(number) {
    return (number < 10)
  }
}

export default Timer
